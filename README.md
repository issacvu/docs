# Sequelize Docs

# Installing

```
$ npm install --save pg pg-hstore # Postgres
```

# Model Basics

## Using `sequelize.define`

```js
const { Sequelize, DataTypes } = require("sequelize");
const sequelize = new Sequelize("sqlite::memory:");

const User = sequelize.define(
  "User",
  {
    // Model attributes are defined here
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastName: {
      type: DataTypes.STRING,
      // allowNull defaults to true
    },
  },
  {
    // Other model options go here
  }
);

// `sequelize.define` also returns the model
console.log(User === sequelize.models.User); // true
```

## Extending Model

```js
const { Sequelize, DataTypes, Model } = require("sequelize");
const sequelize = new Sequelize("sqlite::memory:");

class User extends Model {}

User.init(
  {
    // Model attributes are defined here
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastName: {
      type: DataTypes.STRING,
      // allowNull defaults to true
    },
  },
  {
    // Other model options go here
    sequelize, // We need to pass the connection instance
    modelName: "User", // We need to choose the model name
  }
);

// the defined model is the class itself
console.log(User === sequelize.models.User); // true
```

## Synchronizing all models at once

Use `sequelize.sync()` to automatically synchronize all models

```js
await sequelize.sync();
console.log("All models were synchronized successfully.");
```

## Default Values

```js
sequelize.define("User", {
  name: {
    type: DataTypes.STRING,
    defaultValue: "Jack",
  },
});
```

# Model Instances

## Creating an instance

### A very useful shortcut: the `create` method:

```js
const jane = await User.create({ name: "Jane" });
// Jane exists in the database now!
console.log(jane instanceof User); // true
console.log(jane.name); // "Jane"
```

### Note: logging instances

```js
const jane = await User.create({ name: "Jane" });
// console.log(jane); // Don't do this
console.log(jane.toJSON()); // This is good!
console.log(JSON.stringify(jane, null, 4)); // This is also good!
```

## Updating an instance

Calling `save` again will update it accordingly:

```js
const jane = await User.create({ name: "Jane" });
console.log(jane.name); // "Jane"
jane.name = "Ada";
// the name is still "Jane" in the database
await jane.save();
// Now the name was updated to "Ada" in the database!
```

Can update several fields at once with the `set` method:

```js
const jane = await User.create({ name: "Jane" });

jane.set({
  name: "Ada",
  favoriteColor: "blue",
});
// As above, the database still has "Jane" and "green"
await jane.save();
// The database now has "Ada" and "blue" for name and favorite color
```

Note that the `save()` here will also persist any other changes that have been made on this instance, not just those in the previous `set` call. If you want to update a specific set of fields, you can use `update`:

```js
const jane = await User.create({ name: "Jane" });
jane.favoriteColor = "blue";
await jane.update({ name: "Ada" });
// The database now has "Ada" for name, but still has the default "green" for favorite color
await jane.save();
// Now the database has "Ada" for name and "blue" for favorite color
```

## Deleting an instance

Can delete an instance by calling `destroy`:

```js
const jane = await User.create({ name: "Jane" });
console.log(jane.name); // "Jane"
await jane.destroy();
// Now this entry was removed from the database
```

# Model Querying - Basics

Make the standard CRUD queries.

## Simple INSERT queries

```js
// Create a new user
const jane = await User.create({ firstName: "Jane", lastName: "Doe" });
console.log("Jane's auto-generated ID:", jane.id);
```

## Simple SELECT queries

```js
// Find all users
const users = await User.findAll();
console.log(users.every((user) => user instanceof User)); // true
console.log("All users:", JSON.stringify(users, null, 2));
```

```sql
SELECT * FROM ...
```

## Specifying attributes for SELECT queries

```js
Model.findAll({
  attributes: ["id", "name"],
});
```

```sql
SELECT id, name FROM ...
```

Use `sequelize.fn` to do aggregations:

```js
Model.findAll({
  attributes: [
    "foo",
    [sequelize.fn("COUNT", sequelize.col("hats")), "n_hats"],
    "bar",
  ],
});
```

```sql
SELECT foo, COUNT(hats) AS n_hats, bar FROM ...
```

Sometimes it may be tiresome to list all the attributes of the model if you only want to add an aggregation:

```js
// This is a tiresome way of getting the number of hats (along with every column)
Model.findAll({
  attributes: [
    "id",
    "foo",
    "bar",
    "baz",
    "qux",
    "hats", // We had to list all attributes...
    [sequelize.fn("COUNT", sequelize.col("hats")), "n_hats"], // To add the aggregation...
  ],
});

// This is shorter, and less error prone because it still works if you add / remove attributes from your model later
Model.findAll({
  attributes: {
    include: [[sequelize.fn("COUNT", sequelize.col("hats")), "n_hats"]],
  },
});
```

## Applying WHERE clauses

### The basics

```js
Post.findAll({
  where: {
    authorId: 2,
  },
});
// SELECT * FROM post WHERE authorId = 2;
```

```js
const { Op } = require("sequelize");
Post.findAll({
  where: {
    authorId: {
      [Op.eq]: 2,
    },
  },
});
// SELECT * FROM post WHERE authorId = 2;
```

```js
Post.findAll({
  where: {
    authorId: 12,
    status: "active",
  },
});
// SELECT * FROM post WHERE authorId = 12 AND status = 'active';
```

```js
const { Op } = require("sequelize");
Post.findAll({
  where: {
    [Op.and]: [{ authorId: 12 }, { status: "active" }],
  },
});
// SELECT * FROM post WHERE authorId = 12 AND status = 'active';
```

```js
const { Op } = require("sequelize");
Post.findAll({
  where: {
    [Op.or]: [{ authorId: 12 }, { authorId: 13 }],
  },
});
// SELECT * FROM post WHERE authorId = 12 OR authorId = 13;
```

### Operators

```js
const { Op } = require("sequelize");
Post.findAll({
  where: {
    [Op.and]: [{ a: 5 }, { b: 6 }],            // (a = 5) AND (b = 6)
    [Op.or]: [{ a: 5 }, { b: 6 }],             // (a = 5) OR (b = 6)
    someAttribute: {
      // Basics
      [Op.eq]: 3,                              // = 3
      [Op.ne]: 20,                             // != 20
      [Op.is]: null,                           // IS NULL
      [Op.not]: true,                          // IS NOT TRUE
      [Op.or]: [5, 6],                         // (someAttribute = 5) OR (someAttribute = 6)

      // Using dialect specific column identifiers (PG in the following example):
      [Op.col]: 'user.organization_id',        // = "user"."organization_id"

      // Number comparisons
      [Op.gt]: 6,                              // > 6
      [Op.gte]: 6,                             // >= 6
      [Op.lt]: 10,                             // < 10
      [Op.lte]: 10,                            // <= 10
      [Op.between]: [6, 10],                   // BETWEEN 6 AND 10
      [Op.notBetween]: [11, 15],               // NOT BETWEEN 11 AND 15

      // Other operators

      [Op.all]: sequelize.literal('SELECT 1'), // > ALL (SELECT 1)

      [Op.in]: [1, 2],                         // IN [1, 2]
      [Op.notIn]: [1, 2],                      // NOT IN [1, 2]

      [Op.like]: '%hat',                       // LIKE '%hat'
      [Op.notLike]: '%hat',                    // NOT LIKE '%hat'
      [Op.startsWith]: 'hat',                  // LIKE 'hat%'
      [Op.endsWith]: 'hat',                    // LIKE '%hat'
      [Op.substring]: 'hat',                   // LIKE '%hat%'
      [Op.iLike]: '%hat',                      // ILIKE '%hat' (case insensitive) (PG only)
      [Op.notILike]: '%hat',                   // NOT ILIKE '%hat'  (PG only)
      [Op.regexp]: '^[h|a|t]',                 // REGEXP/~ '^[h|a|t]' (MySQL/PG only)
      [Op.notRegexp]: '^[h|a|t]',              // NOT REGEXP/!~ '^[h|a|t]' (MySQL/PG only)
      [Op.iRegexp]: '^[h|a|t]',                // ~* '^[h|a|t]' (PG only)
      [Op.notIRegexp]: '^[h|a|t]',             // !~* '^[h|a|t]' (PG only)

      [Op.any]: [2, 3],                        // ANY (ARRAY[2, 3]::INTEGER[]) (PG only)
      [Op.match]: Sequelize.fn('to_tsquery', 'fat & rat') // match text search for strings 'fat' and 'rat' (PG only)

      // In Postgres, Op.like/Op.iLike/Op.notLike can be combined to Op.any:
      [Op.like]: { [Op.any]: ['cat', 'hat'] }  // LIKE ANY (ARRAY['cat', 'hat'])

      // There are more postgres-only range operators, see below
    }
  }
});
```

```js
Post.findAll({
  where: {
    id: [1, 2, 3], // Same as using `id: { [Op.in]: [1,2,3] }`
  },
});
// SELECT ... FROM "posts" AS "post" WHERE "post"."id" IN (1, 2, 3);
```

### Logical combinations with operators

```js
const { Op } = require("sequelize");

Foo.findAll({
  where: {
    rank: {
      [Op.or]: {
        [Op.lt]: 1000,
        [Op.eq]: null
      }
    },
    // rank < 1000 OR rank IS NULL

    {
      createdAt: {
        [Op.lt]: new Date(),
        [Op.gt]: new Date(new Date() - 24 * 60 * 60 * 1000)
      }
    },
    // createdAt < [timestamp] AND createdAt > [timestamp]

    {
      [Op.or]: [
        {
          title: {
            [Op.like]: 'Boat%'
          }
        },
        {
          description: {
            [Op.like]: '%boat%'
          }
        }
      ]
    }
    // title LIKE 'Boat%' OR description LIKE '%boat%'
  }
});
```

## Simple UPDATE queries

```js
// Change everyone without a last name to "Doe"
await User.update(
  { lastName: "Doe" },
  {
    where: {
      lastName: null,
    },
  }
);
```

## Simple DELETE queries

// Delete everyone named "Jane"
await User.destroy({
where: {
firstName: "Jane"
}
});

## Creating in bulk

Sequelize provides the `Model.bulkCreate` method to allow creating multiple records at once, with only one query.

```js
const captains = await Captain.bulkCreate([
  { name: "Jack Sparrow" },
  { name: "Davy Jones" },
]);
console.log(captains.length); // 2
console.log(captains[0] instanceof Captain); // true
console.log(captains[0].name); // 'Jack Sparrow'
console.log(captains[0].id); // 1 // (or another auto-generated value)
```

## Ordering and Grouping

### Ordering

```js
Subtask.findAll({
  order: [
    // Will escape title and validate DESC against a list of valid direction parameters
    ["title", "DESC"],

    // Will order by max(age)
    sequelize.fn("max", sequelize.col("age")),

    // Will order by max(age) DESC
    [sequelize.fn("max", sequelize.col("age")), "DESC"],

    // Will order by  otherfunction(`col1`, 12, 'lalala') DESC
    [
      sequelize.fn("otherfunction", sequelize.col("col1"), 12, "lalala"),
      "DESC",
    ],

    // Will order an associated model's createdAt using the model name as the association's name.
    [Task, "createdAt", "DESC"],

    // Will order through an associated model's createdAt using the model names as the associations' names.
    [Task, Project, "createdAt", "DESC"],

    // Will order by an associated model's createdAt using the name of the association.
    ["Task", "createdAt", "DESC"],

    // Will order by a nested associated model's createdAt using the names of the associations.
    ["Task", "Project", "createdAt", "DESC"],

    // Will order by an associated model's createdAt using an association object. (preferred method)
    [Subtask.associations.Task, "createdAt", "DESC"],

    // Will order by a nested associated model's createdAt using association objects. (preferred method)
    [Subtask.associations.Task, Task.associations.Project, "createdAt", "DESC"],

    // Will order by an associated model's createdAt using a simple association object.
    [{ model: Task, as: "Task" }, "createdAt", "DESC"],

    // Will order by a nested associated model's createdAt simple association objects.
    [
      { model: Task, as: "Task" },
      { model: Project, as: "Project" },
      "createdAt",
      "DESC",
    ],
  ],

  // Will order by max age descending
  order: sequelize.literal("max(age) DESC"),

  // Will order by max age ascending assuming ascending is the default order when direction is omitted
  order: sequelize.fn("max", sequelize.col("age")),

  // Will order by age ascending assuming ascending is the default order when direction is omitted
  order: sequelize.col("age"),

  // Will order randomly based on the dialect (instead of fn('RAND') or fn('RANDOM'))
  order: sequelize.random(),
});

Foo.findOne({
  order: [
    // will return `name`
    ["name"],
    // will return `username` DESC
    ["username", "DESC"],
    // will return max(`age`)
    sequelize.fn("max", sequelize.col("age")),
    // will return max(`age`) DESC
    [sequelize.fn("max", sequelize.col("age")), "DESC"],
    // will return otherfunction(`col1`, 12, 'lalala') DESC
    [
      sequelize.fn("otherfunction", sequelize.col("col1"), 12, "lalala"),
      "DESC",
    ],
    // will return otherfunction(awesomefunction(`col`)) DESC, This nesting is potentially infinite!
    [
      sequelize.fn(
        "otherfunction",
        sequelize.fn("awesomefunction", sequelize.col("col"))
      ),
      "DESC",
    ],
  ],
});
```

### Grouping

```js
Project.findAll({ group: "name" });
// yields 'GROUP BY name'
```

## Limits and Pagination

```js
// Fetch 10 instances/rows
Project.findAll({ limit: 10 });

// Skip 8 instances/rows
Project.findAll({ offset: 8 });

// Skip 5 instances and fetch the 5 after that
Project.findAll({ offset: 5, limit: 5 });
```

## Utility methods

### `count`

```js
console.log(`There are ${await Project.count()} projects`);

const amount = await Project.count({
  where: {
    id: {
      [Op.gt]: 25,
    },
  },
});
console.log(`There are ${amount} projects with an id greater than 25`);
```

### `max`, `min` and `sum`

Ages are 10, 5, and 40.

```js
await User.max("age"); // 40
await User.max("age", { where: { age: { [Op.lt]: 20 } } }); // 10
await User.min("age"); // 5
await User.min("age", { where: { age: { [Op.gt]: 5 } } }); // 10
await User.sum("age"); // 55
await User.sum("age", { where: { age: { [Op.gt]: 5 } } }); // 50
```

### `increment`, `decrement`

Age is 10.

```js
await User.increment({ age: 5 }, { where: { id: 1 } }); // Will increase age to 15
await User.increment({ age: -5 }, { where: { id: 1 } }); // Will decrease age to 5
```

# Model Querying - Finders

## `findAll`

```js
const users = await User.findAll();
console.log("All users:", JSON.stringify(users, null, 2));
```

## `findByPk`

```js
const project = await Project.findByPk(123);
if (project === null) {
  console.log("Not found!");
} else {
  console.log(project instanceof Project); // true
  // Its primary key is 123
}
```

## `findOne`

```js
const project = await Project.findOne({ where: { title: "My Title" } });
if (project === null) {
  console.log("Not found!");
} else {
  console.log(project instanceof Project); // true
  console.log(project.title); // 'My Title'
}
```

## `findOrCreate`

```js
const [user, created] = await User.findOrCreate({
  where: { username: "sdepold" },
  defaults: {
    job: "Technical Lead JavaScript",
  },
});
console.log(user.username); // 'sdepold'
console.log(user.job); // This may or may not be 'Technical Lead JavaScript'
console.log(created); // The boolean indicating whether this instance was just created
if (created) {
  console.log(user.job); // This will certainly be 'Technical Lead JavaScript'
}
```

## `findAndCountAll`

```js
const { count, rows } = await Project.findAndCountAll({
  where: {
    title: {
      [Op.like]: "foo%",
    },
  },
  offset: 10,
  limit: 2,
});
console.log(count);
console.log(rows);
```

# Getters, Setters & Virtuals

## Getters

```js
const User = sequelize.define("user", {
  // Let's say we wanted to see every username in uppercase, even
  // though they are not necessarily uppercase in the database itself
  username: {
    type: DataTypes.STRING,
    get() {
      const rawValue = this.getDataValue("username");
      return rawValue ? rawValue.toUpperCase() : null;
    },
  },
});
```

```js
const user = User.build({ username: "SuperUser123" });
console.log(user.username); // 'SUPERUSER123'
console.log(user.getDataValue("username")); // 'SuperUser123'
```

## Setters

```js
const User = sequelize.define("user", {
  username: DataTypes.STRING,
  password: {
    type: DataTypes.STRING,
    set(value) {
      // Storing passwords in plaintext in the database is terrible.
      // Hashing the value with an appropriate cryptographic hash function is better.
      this.setDataValue("password", hash(value));
    },
  },
});
```

```js
const user = User.build({
  username: "someone",
  password: "NotSo§tr0ngP4$SW0RD!",
});
console.log(user.password); // '7cfc84b8ea898bb72462e78b4643cfccd77e9f05678ec2ce78754147ba947acc'
console.log(user.getDataValue("password")); // '7cfc84b8ea898bb72462e78b4643cfccd77e9f05678ec2ce78754147ba947acc'
```

## Combining getters and setters

```js
const { gzipSync, gunzipSync } = require("zlib");

const Post = sequelize.define("post", {
  content: {
    type: DataTypes.TEXT,
    get() {
      const storedValue = this.getDataValue("content");
      const gzippedBuffer = Buffer.from(storedValue, "base64");
      const unzippedBuffer = gunzipSync(gzippedBuffer);
      return unzippedBuffer.toString();
    },
    set(value) {
      const gzippedBuffer = gzipSync(value);
      this.setDataValue("content", gzippedBuffer.toString("base64"));
    },
  },
});
```

## Virtual fields

```js
const { DataTypes } = require("sequelize");

const User = sequelize.define("user", {
  firstName: DataTypes.TEXT,
  lastName: DataTypes.TEXT,
  fullName: {
    type: DataTypes.VIRTUAL,
    get() {
      return `${this.firstName} ${this.lastName}`;
    },
    set(value) {
      throw new Error("Do not try to set the `fullName` value!");
    },
  },
});
```

The `VIRTUAL` field does not cause a column in the table to exist. In other words, the model above will not have a `fullName` column

```js
const user = await User.create({ firstName: "John", lastName: "Doe" });
console.log(user.fullName); // 'John Doe'
```

# Validations & Constraints

```js
const { Sequelize, Op, Model, DataTypes } = require("sequelize");
const sequelize = new Sequelize("sqlite::memory:");

const User = sequelize.define("user", {
  username: {
    type: DataTypes.TEXT,
    allowNull: false,
    unique: true,
  },
  hashedPassword: {
    type: DataTypes.STRING(64),
    validate: {
      is: /^[0-9a-f]{64}$/i,
    },
  },
});

(async () => {
  await sequelize.sync({ force: true });
  // Code here
})();
```

```js
sequelize.define('foo', {
  bar: {
    type: DataTypes.STRING,
    validate: {
      is: /^[a-z]+$/i,          // matches this RegExp
      is: ["^[a-z]+$",'i'],     // same as above, but constructing the RegExp from a string
      not: /^[a-z]+$/i,         // does not match this RegExp
      not: ["^[a-z]+$",'i'],    // same as above, but constructing the RegExp from a string
      isEmail: true,            // checks for email format (foo@bar.com)
      isUrl: true,              // checks for url format (https://foo.com)
      isIP: true,               // checks for IPv4 (129.89.23.1) or IPv6 format
      isIPv4: true,             // checks for IPv4 (129.89.23.1)
      isIPv6: true,             // checks for IPv6 format
      isAlpha: true,            // will only allow letters
      isAlphanumeric: true,     // will only allow alphanumeric characters, so "_abc" will fail
      isNumeric: true,          // will only allow numbers
      isInt: true,              // checks for valid integers
      isFloat: true,            // checks for valid floating point numbers
      isDecimal: true,          // checks for any numbers
      isLowercase: true,        // checks for lowercase
      isUppercase: true,        // checks for uppercase
      notNull: true,            // won't allow null
      isNull: true,             // only allows null
      notEmpty: true,           // don't allow empty strings
      equals: 'specific value', // only allow a specific value
      contains: 'foo',          // force specific substrings
      notIn: [['foo', 'bar']],  // check the value is not one of these
      isIn: [['foo', 'bar']],   // check the value is one of these
      notContains: 'bar',       // don't allow specific substrings
      len: [2,10],              // only allow values with length between 2 and 10
      isUUID: 4,                // only allow uuids
      isDate: true,             // only allow date strings
      isAfter: "2011-11-05",    // only allow date strings after a specific date
      isBefore: "2011-11-05",   // only allow date strings before a specific date
      max: 23,                  // only allow values <= 23
      min: 23,                  // only allow values >= 23
      isCreditCard: true,       // check for valid credit card numbers

      // Examples of custom validators:
      isEven(value) {
        if (parseInt(value) % 2 !== 0) {
          throw new Error('Only even values are allowed!');
        }
      }
      isGreaterThanOtherField(value) {
        if (parseInt(value) <= parseInt(this.otherField)) {
          throw new Error('Bar must be greater than otherField.');
        }
      }
    }
  }
});
```

# Raw Queries

### Use the `sequelize.query` method.

```js
const [results, metadata] = await sequelize.query(
  "UPDATE users SET y = 42 WHERE x = 12"
);
// Results will be an empty array and metadata will contain the number of affected rows.
```

```js
const { QueryTypes } = require("sequelize");
const users = await sequelize.query("SELECT * FROM `users`", {
  type: QueryTypes.SELECT,
});
// We didn't need to destructure the result here - the results were returned directly
```

```js
const { QueryTypes } = require("sequelize");

await sequelize.query("SELECT * FROM projects WHERE status = :status", {
  replacements: { status: "active" },
  type: QueryTypes.SELECT,
});
```

# Associations

To do this, `Sequelize` provides four types of associations that should be combined to create them:

- The `HasOne` association
- The `BelongsTo` association
- The `HasMany` association
- The `BelongsToMany` association

## Defining the Sequelize associations

```js
const A = sequelize.define("A" /* ... */);
const B = sequelize.define("B" /* ... */);

A.hasOne(B); // A HasOne B
A.belongsTo(B); // A BelongsTo B
A.hasMany(B); // A HasMany B
A.belongsToMany(B, { through: "C" }); // A BelongsToMany B through the junction table C
```

## Eager loading (https://sequelize.org/docs/v6/advanced-association-concepts/eager-loading/)
Use of the attribute `include` when you are calling `find` or `findAll`. Lets assume the following setup:
```js
var User = sequelize.define('user', { name: Sequelize.STRING })
  , Task = sequelize.define('task', { name: Sequelize.STRING })
  , Tool = sequelize.define('tool', { name: Sequelize.STRING })

Task.belongsTo(User)
User.hasMany(Task)
User.hasMany(Tool, { as: 'Instruments' })
```

```js
Task.findAll({ include: [ User ] }).then(function(tasks) {
  console.log(JSON.stringify(tasks))

  /*
    [{
      "name": "A Task",
      "id": 1,
      "createdAt": "2013-03-20T20:31:40.000Z",
      "updatedAt": "2013-03-20T20:31:40.000Z",
      "userId": 1,
      "user": {
        "name": "John Doe",
        "id": 1,
        "createdAt": "2013-03-20T20:31:45.000Z",
        "updatedAt": "2013-03-20T20:31:45.000Z"
      }
    }]
  */
})
```
```js
User.findAll({ include: [ Task ] }).then(function(users) {
  console.log(JSON.stringify(users))

  /*
    [{
      "name": "John Doe",
      "id": 1,
      "createdAt": "2013-03-20T20:31:45.000Z",
      "updatedAt": "2013-03-20T20:31:45.000Z",
      "tasks": [{
        "name": "A Task",
        "id": 1,
        "createdAt": "2013-03-20T20:31:40.000Z",
        "updatedAt": "2013-03-20T20:31:40.000Z",
        "userId": 1
      }]
    }]
  */
})
```

```js
// Inner where, with default `required: true`
await User.findAll({
  include: {
    model: Tool,
    as: 'Instruments',
    where: {
      size: { [Op.ne]: 'small' }
    }
  }
});

// Inner where, `required: false`
await User.findAll({
  include: {
    model: Tool,
    as: 'Instruments',
    where: {
      size: { [Op.ne]: 'small' }
    },
    required: false
  }
});

// Top-level where, with default `required: false`
await User.findAll({
  where: {
    '$Instruments.size$': { [Op.ne]: 'small' }
  },
  include: {
    model: Tool,
    as: 'Instruments'
  }
});

// Top-level where, `required: true`
await User.findAll({
  where: {
    '$Instruments.size$': { [Op.ne]: 'small' }
  },
  include: {
    model: Tool,
    as: 'Instruments',
    required: true
  }
});
```

Generated SQLs, in order:
```sql
-- Inner where, with default `required: true`
SELECT [...] FROM `users` AS `user`
INNER JOIN `tools` AS `Instruments` ON
  `user`.`id` = `Instruments`.`userId`
  AND `Instruments`.`size` != 'small';

-- Inner where, `required: false`
SELECT [...] FROM `users` AS `user`
LEFT OUTER JOIN `tools` AS `Instruments` ON
  `user`.`id` = `Instruments`.`userId`
  AND `Instruments`.`size` != 'small';

-- Top-level where, with default `required: false`
SELECT [...] FROM `users` AS `user`
LEFT OUTER JOIN `tools` AS `Instruments` ON
  `user`.`id` = `Instruments`.`userId`
WHERE `Instruments`.`size` != 'small';

-- Top-level where, `required: true`
SELECT [...] FROM `users` AS `user`
INNER JOIN `tools` AS `Instruments` ON
  `user`.`id` = `Instruments`.`userId`
WHERE `Instruments`.`size` != 'small';
```

## Special methods/mixins added to instances

### `Foo.hasOne(Bar)`

- `fooInstance.getBar()`
- `fooInstance.setBar()`
- `fooInstance.createBar()`

```js
const foo = await Foo.create({ name: "the-foo" });
const bar1 = await Bar.create({ name: "some-bar" });
const bar2 = await Bar.create({ name: "another-bar" });
console.log(await foo.getBar()); // null
await foo.setBar(bar1);
console.log((await foo.getBar()).name); // 'some-bar'
await foo.createBar({ name: "yet-another-bar" });
const newlyAssociatedBar = await foo.getBar();
console.log(newlyAssociatedBar.name); // 'yet-another-bar'
await foo.setBar(null); // Un-associate
console.log(await foo.getBar()); // null
```

### `Foo.belongsTo(Bar)`

The same ones from `Foo.hasOne(Bar)`:

- `fooInstance.getBar()`
- `fooInstance.setBar()`
- `fooInstance.createBar()`

### `Foo.hasMany(Bar)`

- `fooInstance.getBars()`
- `fooInstance.countBars()`
- `fooInstance.hasBar()`
- `fooInstance.hasBars()`
- `fooInstance.setBars()`
- `fooInstance.addBar()`
- `fooInstance.addBars()`
- `fooInstance.removeBar()`
- `fooInstance.removeBars()`
- `fooInstance.createBar()`

```js
const foo = await Foo.create({ name: "the-foo" });
const bar1 = await Bar.create({ name: "some-bar" });
const bar2 = await Bar.create({ name: "another-bar" });
console.log(await foo.getBars()); // []
console.log(await foo.countBars()); // 0
console.log(await foo.hasBar(bar1)); // false
await foo.addBars([bar1, bar2]);
console.log(await foo.countBars()); // 2
await foo.addBar(bar1);
console.log(await foo.countBars()); // 2
console.log(await foo.hasBar(bar1)); // true
await foo.removeBar(bar2);
console.log(await foo.countBars()); // 1
await foo.createBar({ name: "yet-another-bar" });
console.log(await foo.countBars()); // 2
await foo.setBars([]); // Un-associate all previously associated bars
console.log(await foo.countBars()); // 0
```

### `Foo.belongsToMany(Bar, { through: Baz })`

The same ones from `Foo.hasMany(Bar)`:

- `fooInstance.getBars()`
- `fooInstance.countBars()`
- `fooInstance.hasBar()`
- `fooInstance.hasBars()`
- `fooInstance.setBars()`
- `fooInstance.addBar()`
- `fooInstance.addBars()`
- `fooInstance.removeBar()`
- `fooInstance.removeBars()`
- `fooInstance.createBar()`

## Creating associations referencing a field which is not the primary key

### For `belongsTo` relationships

```js
const Ship = sequelize.define(
  "ship",
  { name: DataTypes.TEXT },
  { timestamps: false }
);
const Captain = sequelize.define(
  "captain",
  {
    name: { type: DataTypes.TEXT, unique: true },
  },
  { timestamps: false }
);

Ship.belongsTo(Captain, { targetKey: "name", foreignKey: "captainName" });
// This creates a foreign key called `captainName` in the source model (Ship)
// which references the `name` field from the target model (Captain).

await Captain.create({ name: "Jack Sparrow" });
const ship = await Ship.create({
  name: "Black Pearl",
  captainName: "Jack Sparrow",
});
console.log((await ship.getCaptain()).name); // "Jack Sparrow"
```

### For `hasOne` and `hasMany` relationships

```js
const Foo = sequelize.define(
  "foo",
  {
    name: { type: DataTypes.TEXT, unique: true },
  },
  { timestamps: false }
);
const Bar = sequelize.define(
  "bar",
  {
    title: { type: DataTypes.TEXT, unique: true },
  },
  { timestamps: false }
);
const Baz = sequelize.define(
  "baz",
  { summary: DataTypes.TEXT },
  { timestamps: false }
);
Foo.hasOne(Bar, { sourceKey: "name", foreignKey: "fooName" });
Bar.hasMany(Baz, { sourceKey: "title", foreignKey: "barTitle" });
// [...]
await Bar.setFoo("Foo's Name Here");
await Baz.addBar("Bar's Title Here");
```

For `belongsToMany` relationships

```js
const Foo = sequelize.define(
  "foo",
  {
    name: { type: DataTypes.TEXT, unique: true },
  },
  { timestamps: false }
);
const Bar = sequelize.define(
  "bar",
  {
    title: { type: DataTypes.TEXT, unique: true },
  },
  { timestamps: false }
);

Foo.belongsToMany(Bar, { through: "foo_bar" });
// This creates a junction table `foo_bar` with fields `fooId` and `barId`

Foo.belongsToMany(Bar, { through: "foo_bar", targetKey: "title" });
// This creates a junction table `foo_bar` with fields `fooId` and `barTitle`

Foo.belongsToMany(Bar, { through: "foo_bar", sourceKey: "name" });
// This creates a junction table `foo_bar` with fields `fooName` and `barId`

Foo.belongsToMany(Bar, {
  through: "foo_bar",
  sourceKey: "name",
  targetKey: "title",
});
// This creates a junction table `foo_bar` with fields `fooName` and `barTitle`
```

# Other topics

https://sequelize.org/docs/v6/category/other-topics/
